import os

# General config
FRONTEND_URL = "https://www.forestfyre.xyz"

# Database settings (see https://docs.sqlalchemy.org/en/latest/core/engines.html#database-urls)
DB_TYPE = os.environ.get("APP_DB_TYPE", "sqlite")
DB_NAME = os.environ.get("APP_DB_NAME", "development.db")
DB_HOST = os.environ.get("APP_DB_HOST", None)
DB_USER = os.environ.get("APP_DB_USER", None)
DB_PASSWD = os.environ.get("APP_DB_PASSWD", None)
DB_PORT = os.environ.get("APP_DB_PORT", None)

# Development settings
DEBUG = True if os.environ.get("APP_DEBUG") in ("TRUE", "True", "true", "1") else False

# Read version string (configured by CI)
with open("version.txt") as f:
    VERSION = f.read().rstrip()
