import time

from sqlalchemy import create_engine, func, and_
from sqlalchemy.engine.url import URL
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import scoped_session, sessionmaker

import settings

engine = create_engine(
    URL(
        drivername=settings.DB_TYPE,
        database=settings.DB_NAME,
        host=settings.DB_HOST,
        username=settings.DB_USER,
        password=settings.DB_PASSWD,
        port=settings.DB_PORT,
    ),
    echo=settings.DEBUG,
)

db_session = scoped_session(
    sessionmaker(autocommit=False, autoflush=False, bind=engine)
)

Base = declarative_base()
Base.query = db_session.query_property()


def init_db():
    import models  # noqa: F401

    Base.metadata.create_all(bind=engine)


def populate_sample_data():
    from models import Fire

    now = int(time.time())
    session = db_session()
    session.add_all(
        [
            Fire(
                identifier=1,
                latitude=50.024444,
                longitude=-125.2475,
                radius=1.5,
                note="probably hot here",
                timestamp=now - 1000000,
            ),
            Fire(
                identifier=2,
                latitude=49.143889,
                longitude=-125.891667,
                radius=1.5,
                note="maybe avoid this",
                timestamp=now - 1000000,
            ),
            Fire(
                identifier=3,
                latitude=49.753056,
                longitude=-125.296389,
                radius=1.5,
                note="has anyone called 911?",
                timestamp=now - 1000000,
            ),
        ]
    )
    session.commit()


def select_fires(start_timestamp, end_timestamp):
    from models import Fire

    session = db_session()
    ids = (
        session.query(Fire.identifier, func.max(Fire.timestamp).label("timestamp"))
        .filter(Fire.timestamp >= start_timestamp)
        .filter(Fire.timestamp <= end_timestamp)
        .filter(Fire.radius != 0)
        .group_by(Fire.identifier)
        .subquery()
    )
    print([x for x in ids.c])
    fires = [
        x.as_dict()
        for x in Fire.query.join(
            ids,
            and_(
                Fire.identifier == ids.c.identifier, Fire.timestamp == ids.c.timestamp
            ),
        ).all()
    ]
    return fires
