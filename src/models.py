from sqlalchemy import Column, Float, Integer, String

from database import Base


class Fire(Base):
    def as_dict(self):
        return {c.name: getattr(self, c.name) for c in self.__table__.columns}

    __tablename__ = "fires"

    id = Column(Integer, primary_key=True)
    identifier = Column(Integer)
    latitude = Column(Float)
    longitude = Column(Float)
    radius = Column(Float)
    timestamp = Column(Integer)
    note = Column(String)

    def __repr__(self):
        return "".join(
            [
                f"<Fire("
                f"id='{self.id}', "
                f"identifier='{self.identifier}', "
                f"latitude='{self.latitude}', "
                f"longitude='{self.longitude}', "
                f"radius='{self.radius}', "
                f"timstamp='{self.timestamp}', "
                f"note='{self.note}')>"
            ]
        )
