import logging
import sys
import time
from datetime import datetime, timedelta

from flask import Flask, request, url_for
from flask_api import status
from flask_restplus import Resource, Api, fields

import database
import settings
from database import db_session


class ProxyApi(Api):
    @property
    def specs_url(self):
        scheme = "http" if "5000" in self.base_url else "https"
        return url_for(self.endpoint("specs"), _external=True, _scheme=scheme)


logging.basicConfig(stream=sys.stderr, level=logging.INFO)
app = Flask(__name__)
api = ProxyApi(
    app,
    version="1.0",
    title="ForestFyre API",
    description="Backend API for ForestFyre (https://www.forestfyre.xyz)",
).namespace("/", description="All routes")

coord = api.model(
    "coord",
    {
        "identifier": fields.Integer(description="Fire identifier", example=1),
        "lat": fields.Float(description="Latitude coordinate", example=50.024444),
        "lng": fields.Float(description="Longitude coordinate", example=-125.2475),
        "radius": fields.Float(description="Fire radius", example=1.5),
        "note": fields.String(
            description="Specific notes related to the fire",
            example="Spreading rapidly, no signs of slowing down",
        ),
        "timestamp": fields.Integer(
            description="Observation time (Unix timestamp)", example=1552291295
        ),
    },
)

fires_response = api.model(
    "fires",
    {
        "metadata": fields.String(description="Human readable informational string"),
        "coords": fields.List(fields.Nested(coord)),
    },
)

database.init_db()
if settings.DEBUG and settings.DB_TYPE == "sqlite":
    database.populate_sample_data()


@app.teardown_appcontext
def shutdown_session(exception=None):
    db_session.remove()


@app.after_request
def apply_headers(response):
    """Add cache headers to all responses"""
    response.headers["Cache-Control"] = "no-cache"
    response.headers["Access-Control-Allow-Origin"] = settings.FRONTEND_URL
    return response


@api.route("/ping")
class Ping(Resource):
    def get(self):
        """
        Heartbeat check
        Always returns a version string with status code 200. Any other status code indicates an error.
        """
        return (f"Okay ({settings.VERSION})", status.HTTP_200_OK)


@api.route("/fires")
@api.doc(
    params={
        "start_timestamp": "Start time for requested time range (Unix timestamp). Defaults to 24 hours ago.",
        "end_timeshamp": "End time for requested time range (Unix timestamp). Defaults to the current time.",
    }
)
class Fires(Resource):
    @api.doc(model=fires_response)
    def get(self):
        """
        Returns a list of fires
        Takes two optional URL parameters, `start_timestamp` and `end_timestamp`, which determine a timerange to query (defaults to the last 24 hours).
        If multiple observations of a single fire (uniquely identified by its `identifier` field) occurred within the specified time range, only the latest will be returned.
        """

        now_timestamp = int(time.time())
        yesterday_timestamp = int(
            datetime.timestamp(
                datetime.utcfromtimestamp(now_timestamp) - timedelta(days=1)
            )
        )

        start_timestamp = request.args.get(
            "start_timestamp", default=yesterday_timestamp
        )
        end_timestamp = request.args.get("end_timestamp", default=now_timestamp)

        fires = database.select_fires(start_timestamp, end_timestamp)
        for x in fires:
            x["lat"] = x["latitude"]
            x["lng"] = x["longitude"]
            del x["latitude"]
            del x["longitude"]

        result = {
            "metadata": f"Sample fires spotted between {start_timestamp} and {end_timestamp}",
            "coords": fires,
        }
        return result


if __name__ == "__main__":
    # Run Flask application
    app.run(debug=settings.DEBUG)
