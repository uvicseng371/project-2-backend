# Project 2 Backend
## Summary 
This is the backend API for the [ForestFyre](https://www.forestfyre.xyz) application. See [Project 2](https://gitlab.com/uvicseng371/project-2) for a description of the entire project.

The live API is running on Azure and is available at [https://api.forestfyre.xyz](https://api.forestfyre.xyz).

This app acts as an API for the [frontend](https://gitlab.com/uvicseng371/project-2-frontend). The main route is `/fires` which returns JSON-formatted data about the fires to display. More information is available in the built-in API documentation, which can be found [here](https://api.forestfyre.xyz) (expand the *Application API* section).

An overview of the Continuous Integration for this project can be found [here](https://gitlab.com/uvicseng371/project-2-backend/snippets/1845881).


## Running an Instance Locally
1. Ensure you have Python 3.6+ on your machine
2. Clone the repository with `git clone https://gitlab.com/uvicseng371/project-2-backend.git`
3. Enter the repo with `cd project-2-backend`
4. Create a virtual environment for dependencies using `python3 -m venv .venv` or `virtualenv -p $(which python3) .venv`
5. Activate your virtual environment with `source .venv/bin/activate` (macOS/Linux) or `.venv/Scripts/activate.bat` (Windows)
6. Install the dependencies with `pip install -r requirements.txt`
7. Run the web server in production mode with `gunicorn --bind 0.0.0.0 --chdir src app:app`
8. Test the server by visting `http://localhost:8000/ping` in a web browser. You should see a message that says `Okay (development)`.

## Setting Up a Development Environment
1. Ensure you have Python 3.6+ on your machine
2. Clone the repository with `git clone https://gitlab.com/uvicseng371/project-2-backend.git`
3. Enter the repo with `cd project-2-backend`
4. Create a virtual environment for dependencies using `python3 -m venv .venv` or `virtualenv -p $(which python3) .venv`
5. Activate your virtual environment with `source .venv/bin/activate` (macOS/Linux) or `.venv/Scripts/activate.bat` (Windows)
6. Install the development dependencies with `pip install -r requirements-dev.txt`
7. Enter the `src` directory and run the web server in develop mode with `cd src; python app.py`
8. Test the server by visting `http://localhost:5000/ping` in a web browser. You should see a message that says `Okay (development)`.
9. _(Optional, recommended)_ Install the pre-commit hooks with `pre-commit install` to enable code checks
